PHONY: all clean dependencies

.SUFFIXES: .ts .js .min.js

MINIFY := minify.js

all: clean dependencies minify.js

clean:
	$(RM) *.js
	$(RM) package-lock.json
	$(RM) -r node_modules

dependencies:
	npm i @types/node

.ts.js: dependencies
	tsc --strict $<

.js.min.js: $(MINIFY)
	node $(MINIFY) $< > $@
