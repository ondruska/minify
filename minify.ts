import * as https from 'https';
import * as fs from 'fs';
import * as qs from 'querystring';

let encoding = 'utf8';

if (process.argv.length !== 3) process.exit();

fs.readFile(process.argv[process.argv.length - 1], encoding, (e, d) => {

	let post = qs.stringify({
		'compilation_level': 'SIMPLE_OPTIMIZATIONS' // WHITESPACE_ONLY SIMPLE_OPTIMIZATIONS
		, 'output_format': 'text'
		, 'output_info': 'compiled_code'
		, 'js_code': d
	});

	let options = {
		hostname: 'closure-compiler.appspot.com'
		, path: '/compile'
		, method: 'POST'
		, headers: {
			'Content-type': 'application/x-www-form-urlencoded'
		}
	}

	let req = https.request(options, (res) => {
		res.setEncoding(encoding);
		res.on('data', (data) => {
			console.log(data);
		});
	});

	req.on('error', (e) => {
		console.error(e);
	});

	req.write(post);

	req.end();

});
